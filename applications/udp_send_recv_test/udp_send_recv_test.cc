// Copyright (c) 2023 Dominic Adam Walters
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

#include <iostream>
#include <string>
#include "OcpiApi.hh"

#include <boost/asio.hpp>

using boost::asio::ip::address;
using boost::asio::ip::udp;

namespace OA = OCPI::API;

boost::asio::io_context _io_context;
udp::socket _socket{_io_context, udp::endpoint(udp::v4(), 12345)};
udp::endpoint _remote_endpoint;
boost::system::error_code err;
uint8_t packets_received = 0;

int main(/*int argc, char **argv*/) {

  try {
    OA::Application app("udp_send_recv_test.xml");
    app.initialize();
    app.start();

    OA::ExternalPort& file_write = app.getPort("file_write_in");

    while (true) {
      OA::ExternalBuffer* b;
      uint8_t* data;
      std::size_t length;
      do
        b = file_write.getOutputBuffer(data, length);
      while (!b);
      std::size_t recv = _socket.receive_from(
        boost::asio::buffer(data, length),
        _remote_endpoint,
        0,
        err
      );
      if (recv != 0) {
        b->put(recv);
        packets_received++;
      }
      if (packets_received == 10) {
        break;
      }
    }

    app.wait(1000);
    app.finish();
  } catch (std::string &e) {
    std::cerr << "app failed: " << e << std::endl;
    return 1;
  }
  return 0;
}
