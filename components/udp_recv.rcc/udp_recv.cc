// Copyright (c) 2023 Dominic Adam Walters
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

#include "udp_recv-worker.hh"

#define BOOST_ASIO_DYN_LINK 1
#include <boost/asio.hpp>

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Udp_recvWorkerTypes;

using boost::asio::ip::address;
using boost::asio::ip::udp;

class Udp_recvWorker : public Udp_recvWorkerBase {

  boost::asio::io_service _io_service;
  std::unique_ptr<udp::socket> _socket;
  std::unique_ptr<udp::endpoint> _remote_endpoint;

  RunCondition _run_condition {1 << UDP_RECV_OUTPUT, RCC_NO_PORTS};

  RCCResult start() {
    this->_socket = std::unique_ptr<udp::socket>(
      new udp::socket(this->_io_service)
    );
    this->_remote_endpoint = std::unique_ptr<udp::endpoint>(
      new udp::endpoint(
        address::from_string(this->m_properties.origination_ip),
        this->m_properties.udp_port
      )
    );
    this->_socket->open(udp::v4());
    this->_socket->non_blocking(true);
    this->setRunCondition(&this->_run_condition);
    this->output.setDefaultLength(this->m_properties.max_payload_size);
    this->output.setDefaultOpCode(0);
    return RCC_OK;
  }

  RCCResult run(bool /*timedout*/) {
    if (!this->output.hasBuffer()) {
      this->output.request();
    }
    boost::system::error_code err;
    std::size_t recv = this->_socket->receive_from(
      boost::asio::buffer(this->output.data(), this->output.length()),
      *this->_remote_endpoint,
      0,
      err
    );
    if (recv == 0 || err == boost::asio::error::would_block) {
      return RCC_OK;
    }
    this->log(0, "Received %zu payload bytes", recv);
    this->output.setLength(recv);
    return RCC_ADVANCE;
  }

};

UDP_RECV_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
// YOU MUST LEAVE THE *START_INFO and *END_INFO macros here and uncommented in any case
UDP_RECV_END_INFO
