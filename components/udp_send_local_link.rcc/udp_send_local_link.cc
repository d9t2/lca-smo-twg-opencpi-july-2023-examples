// Copyright (c) 2023 Dominic Adam Walters
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

#include "udp_send_local_link-worker.hh"

#define BOOST_ASIO_DYN_LINK 1
#include <boost/asio.hpp>

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Udp_send_local_linkWorkerTypes;

using boost::asio::ip::address;
using boost::asio::ip::udp;

class Udp_send_local_linkWorker : public Udp_send_local_linkWorkerBase {

  boost::asio::io_service _io_service;
  std::unique_ptr<udp::socket> _socket;
  std::unique_ptr<udp::endpoint> _remote_endpoint;

  RunCondition _run_condition {1 << UDP_SEND_LOCAL_LINK_INPUT, RCC_NO_PORTS};

  RCCResult start() {
    this->_socket = std::unique_ptr<udp::socket>(
      new udp::socket(this->_io_service)
    );
    this->_remote_endpoint = std::unique_ptr<udp::endpoint>(
      new udp::endpoint(
        address::from_string(this->m_properties.destination_ip),
        this->m_properties.udp_port
      )
    );
    this->_socket->open(udp::v4());
    this->setRunCondition(&this->_run_condition);
    return RCC_OK;
  }

  RCCResult run(bool /*timedout*/) {
    this->log(0, "UDP Send: Start of `run` method");
    if (this->input.eof()) {
      return RCC_ADVANCE_FINISHED;
    }
    if (!this->input.hasBuffer()) {
      return RCC_OK;
    }
    boost::system::error_code err;
    std::size_t sent = this->_socket->send_to(
      boost::asio::buffer(this->input.data(), this->input.length()),
      *this->_remote_endpoint,
      0,
      err
    );
    this->log(0, "Sent %zu payload bytes", sent);
    return RCC_ADVANCE;
  }

};

UDP_SEND_LOCAL_LINK_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
// YOU MUST LEAVE THE *START_INFO and *END_INFO macros here and uncommented in any case
UDP_SEND_LOCAL_LINK_END_INFO
