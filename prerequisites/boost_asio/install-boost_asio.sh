# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

[ -z "$OCPI_CDK_DIR" ] && echo Environment variable OCPI_CDK_DIR not set && exit 1

source $OCPI_CDK_DIR/scripts/setup-prerequisite.sh \
  "$1" \
  boost_asio \
  "Boost Asio Networking Library" \
  https://github.com/boostorg/boost.git \
  boost-1.82.0 \
  boost \
  1

git submodule update --init ../libs/asio
git submodule update --init ../libs/assert
git submodule update --init ../libs/config
git submodule update --init ../libs/exception
git submodule update --init ../libs/regex
git submodule update --init ../libs/system
git submodule update --init ../libs/throw_exception
git submodule update --init ../libs/variant2

${OcpiCrossCompile}gcc \
  -fPIC \
  -c $OcpiThisPrerequisiteDir/boost_asio.cc \
  -DBOOST_ASIO_DYN_LINK \
  -DBOOST_SYSTEM_DYN_LINK \
  -I ../libs/asio/include \
  -I ../libs/assert/include \
  -I ../libs/config/include \
  -I ../libs/exception/include \
  -I ../libs/regex/include \
  -I ../libs/system/include \
  -I ../libs/throw_exception/include \
  -I ../libs/variant2/include \
  -o boost_asio.o
${OcpiCrossCompile}ar \
  rcs \
  libboost_asio.a \
  boost_asio.o
${OcpiCrossCompile}gcc \
  -shared \
  boost_asio.o \
  -lstdc++ \
  -o libboost_asio.so

cp -r ../libs/asio/include "$OcpiInstallDir/."
cp -r ../libs/assert/include "$OcpiInstallDir/."
cp -r ../libs/config/include "$OcpiInstallDir/."
cp -r ../libs/exception/include "$OcpiInstallDir/."
cp -r ../libs/regex/include "$OcpiInstallDir/."
cp -r ../libs/system/include "$OcpiInstallDir/."
cp -r ../libs/throw_exception/include "$OcpiInstallDir/."
cp -r ../libs/variant2/include "$OcpiInstallDir/."
mkdir -p "$OcpiInstallExecDir/lib"
cp -r libboost_asio.a "$OcpiInstallExecDir/lib/."
cp -r libboost_asio.so "$OcpiInstallExecDir/lib/."
